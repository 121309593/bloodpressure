/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simonwoodworth.bloodpressure;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author simon
 */

public class PatientTest {

    private Patient patient;
    private static final double DELTA = 1e-15;

    @BeforeEach
    public void setUp() {
        // Initialize a patient object before each test.
        patient = new Patient("John Doe", 30, 'M', 70, 120, 80);
    }

    @Test
    public void testPatientConstructor() {
        assertEquals("John Doe", patient.getName());
        assertEquals(30, patient.getAge());
        assertEquals('M', patient.getGender());
        assertEquals(70, patient.getHeartRate());
        assertEquals(120, patient.getSBP(), DELTA);
        assertEquals(80, patient.getDBP(), DELTA);
        assertEquals(BloodPressureCalculator.calculateMAP(120, 80), patient.getMAP(), DELTA);
    }

    @Test
    public void testSettersAndGetters() {
        patient.setName("Jane Doe");
        patient.setAge(25);
        patient.setGender('F');
        patient.setHeartRate(75);
        patient.setSBP(130);
        patient.setDBP(85);

        assertEquals("Jane Doe", patient.getName());
        assertEquals(25, patient.getAge());
        assertEquals('F', patient.getGender());
        assertEquals(75, patient.getHeartRate());
        assertEquals(130, patient.getSBP(), DELTA);
        assertEquals(85, patient.getDBP(), DELTA);
        assertEquals(BloodPressureCalculator.calculateMAP(130, 85), patient.getMAP(), DELTA);
    }

    @Test
    public void testMAPCalculation() {
        // Test MAP calculation on construction
        double expectedMAP = BloodPressureCalculator.calculateMAP(120, 80);
        assertEquals(expectedMAP, patient.getMAP(), DELTA);

        // Change SBP and DBP and test if MAP is updated correctly
        patient.setSBP(110);
        patient.setDBP(70);
        expectedMAP = BloodPressureCalculator.calculateMAP(110, 70);
        assertEquals(expectedMAP, patient.getMAP(), DELTA);
    }

    // Add more tests as needed for validation, exceptions, etc.
}
